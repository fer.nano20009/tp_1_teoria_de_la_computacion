



object Gramatica1 {

  def procesar(entrada: List[Char]): Boolean =
    {
      if (entrada.length<2)
        false
      else
      {  
        if (S(0, entrada) == entrada.length)
        true
      else
        false
        
      }
        
      
    
    
    }
  def S(indice: Int, entrada: List[Char]): Int =
    {

      A(T(indice, entrada), entrada)

    }

  def T(indice: Int, entrada: List[Char]): Int =
    {

      B(F(indice, entrada), entrada)

    }

  def A(indice: Int, entrada: List[Char]): Int =
    {

      if (indice < entrada.length && entrada.apply(indice) == '+') {

        A(T(indice + 1, entrada), entrada)

      } else
        indice

    }

  def B(indice: Int, entrada: List[Char]): Int =
    {

      if (indice < entrada.length && entrada.apply(indice) == '*') {

        B(F(indice + 1, entrada), entrada)
      } else
        indice

    }

  def F(indice: Int, entrada: List[Char]): Int =
    {

      if (indice < entrada.length && entrada.apply(indice) == '(') {

        val aux = S(indice + 1, entrada)

        aux + 1

      } else if (indice < entrada.length && indice + 1 < entrada.length && entrada.apply(indice) == 'i' && (entrada.apply(indice + 1) == 'd')) {

        indice + 2
      } else
        indice

    }

}