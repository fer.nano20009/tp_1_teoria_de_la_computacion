
import org.scalatest._










class ListSpec extends UnitSpec 
{
  
    "Ingreso id "must " tiene que devolver true "in 
    {
      assert(Gramatica1.procesar(List('i','d'))==true)
    }
    
     "Ingreso (id) "must " tiene que devolver true "in 
    {
      assert(Gramatica1.procesar(List('(','i','d',')'))==true)
    }
     
     
      "Ingreso (id+id) "must " tiene que devolver true "in 
    {
      assert(Gramatica1.procesar(List('(','i','d','+','i','d',')'))==true)
    }
      
       "Ingreso (id*id) "must " tiene que devolver true "in 
    {
      assert(Gramatica1.procesar(List('(','i','d','*','i','d',')'))==true)
    }
        "Ingreso ((id)) "must " tiene que devolver true "in 
    {
      assert(Gramatica1.procesar(List('(','(','i','d',')',')'))==true)
    }
        "Ingreso ((ad)) "must " tiene que devolver false "in 
    {
      assert(Gramatica1.procesar(List('(','(','a','d',')',')'))==false)
    }    
        
          "Ingreso ((id) "must " tiene que devolver false "in 
    {
      assert(Gramatica1.procesar(List('(','(','i','d',')'))==false)
    }    
           "Ingreso  i"must " tiene que devolver false "in 
    {
      assert(Gramatica1.procesar(List('i'))==false)
    }          
      
    //GRAMATICA 2
           
            "Ingreso  cabb"must " tiene que devolver true "in 
    {
      assert(Gramatica2.procesar(List('c','a','b','b'))==true)
    }              
              "Ingreso  cab"must " tiene que devolver true "in 
    {
      assert(Gramatica2.procesar(List('c','a','b'))==true)
    }     
            "Ingreso  cxb"must " tiene que devolver false "in 
    {
      assert(Gramatica2.procesar(List('c','x','b'))==false)
    }   
                    "Ingreso  a"must " tiene que devolver false "in 
    {
      assert(Gramatica2.procesar(List('a'))==false)
    }  
                    
}